<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('lng');
            $table->double('lat');
            $table->string('title');
            $table->longText('order_details');
            $table->string('phone');
//            $table->string('time')->nullable();
            $table->time('time')->nullable();
            $table->date('date')->nullable();

            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->bigInteger('weight_id')->unsigned()->nullable();
            $table->foreign('weight_id')->references('id')->on('weights')->onUpdate('cascade')->onDelete('cascade');


            $table->enum('status', ['is_confirm','is_not_confirm', 'is_cancle'])->nullable();

            $table->boolean('is_fast')->default(0);
            $table->bigInteger('driver_id')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
