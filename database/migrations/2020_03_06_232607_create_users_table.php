<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('full_name');
            $table->double('current_lng')->nullable();
            $table->double('current_lat')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('photo')->nullable();
            $table->string('front_identification_photo')->nullable();
            $table->string('back_identification_photo')->nullable();
           // $table->boolean('is_admin')->default(0);
            $table->boolean('is_accepted')->default(0);
            $table->enum('user_type', ['is_driver', 'is_user','is_admin']);
            $table->bigInteger('transportation_id')->unsigned()->nullable();
            $table->foreign('transportation_id')->references('id')->on('transportations')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
