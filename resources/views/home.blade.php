@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">                    
                     @if(auth()->user()->user_type == "is_admin")
                    <a href="{{url('admin/routes')}}">Hi Admin</a>
                    @elseif(auth()->user()->user_type == "is_user")
                    <div class=”panel-heading”>Hi User</div>
                    @else
                    <div class=”panel-heading”>Hi Driver</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
