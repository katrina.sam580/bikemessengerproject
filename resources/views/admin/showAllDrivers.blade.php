@include('template.includes.header')
		
		<div class="wrapper d-flex align-items-stretch">
	
            
            @include('template.includes.sidebar')
            

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

    @include('template.includes.navbar')
          
          
          
        <h2 class="mb-4">Drivers</h2>
        
          
                   
              
     <table id="allDriver" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Front Iden Photo</th>
                <th>Back Iden Photo</th>
                <th>Transportation</th>
                <th></th>

            </tr>
        </thead>
        <tbody>
            
                 @foreach($drivers as $driver)
            
        
            <!--
       'full_name', 'email', 'password','phone','photo','front_identification_photo','back_identification_photo'
        ,'is_accepted','user_type','transportation_id'
-->
            
            
            
            <tr>
                <td>
                    {{$driver->full_name}} &nbsp;
                    
                    @if($driver->photo)
                    <img src="{{$driver->photo}}" style="width:80px;height:80px" />
                    @endif
                </td> 
                <td>
                    {{$driver->email}}
                </td>  
                <td>
                    {{$driver->phone}}
                </td> 
                <td>
                     <img src="{{'storage/app/'.$driver->front_identification_photo}}" style="width:80px;height:80px" />
                </td>  
                <td>
                     <img src="{{$driver->back_identification_photo}}" style="width:80px;height:80px" />
                    
                    
                </td> 
                <td>
                    
                         <?php
                              if(($driver->transportation_id()->get()) !== null){
                                  foreach($driver->transportation_id()->get() as $transportation_type){
                                    echo $transportation_type->name; 
                                  }   
                            }
                            ?>     
                                
                    
                </td> 
                
                
                
                
                <td>
              
                    
                    @if($driver->is_accepted != 1)
                    
                           
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-success"
                            data-toggle="modal" 
                            data-target="#acceptDriver{{$driver->id}}"
                            >
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="acceptDriver{{$driver->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Delete driver</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p>Are You Sure To Accept {{$driver->full_name}}  ? </p>
                            </div>
                            <div class="modal-footer">
                                
                                   
                             {!! Form::open(['url' => 'accept_driver/'.$driver->id ]) !!}
                                
                             {!! Form::submit('Yes', ['class'=>' submit btn btn-primary'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
   
                    @endif
                    
                    
                    &nbsp;
                  
                   
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-danger"
                            data-toggle="modal" 
                            data-target="#delDriver{{$driver->id}}"
                            >
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="delDriver{{$driver->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Delete driver</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p>Are You Sure To Delete {{$driver->full_name}}  ? </p>
                            </div>
                            <div class="modal-footer">
                                
                                   
                             {!! Form::open(['url' => 'del_driver/'.$driver->id , 'files' => true,'method'=>'delete']) !!}
                                
                             {!! Form::submit('Yes', ['class'=>' submit btn btn-primary'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
   
                    
                    
                    
                    
                    
                </td>
     
            </tr>  
            
                
                @endforeach
            
                  </tbody>
    </table>
          
          
          
          
          
          
          
          
          
      </div>
		</div>


@include('template.includes.footer')