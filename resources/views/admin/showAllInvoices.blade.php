@include('template.includes.header')
		
		<div class="wrapper d-flex align-items-stretch">
	
            
            @include('template.includes.sidebar')
            

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

    @include('template.includes.navbar')
          
          
          
        <h2 class="mb-4">Invoices</h2>
        
          
                   
              
     <table id="allInvoices" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Title</th>
                <th>Invoice Date</th>
                <th>Total Cost</th>
                <th>Total Fines</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            
                 @foreach($invoices as $invoice)
            
        
            
            
            
            <tr>
            
                <td>
                    
                                <?php
                                       if(($invoice->order_id()->get()) !== null){
                                          foreach($invoice->order_id()->get() as $order){
                                            echo $order->title; 
                                          }   
                                     }
                                    
                                    ?>
                  
                </td> 
                
                <td>
                  <p>
                      {{$invoice->invoice_date}}
                    </p>       
                                     
                
                </td>   
                <td>
                  <p>
                      {{$invoice->total_cost}}
                    </p>       
                                     
                
                </td>  
                
                <td>
                  <p>
                      {{$invoice->total_fines}}
                    </p>       
                                     
                
                </td>
                
                
                
                
                
                
         
                <td>
               
                    
                    
                                <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-primary"
                            data-toggle="modal" 
                            data-target="#viewOrder{{$order->id}}"
                            >
                           <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="viewOrder{{$order->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4> {{$order->title}} </h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p> details :</p> <textarea readonly style="width:100%;min-height:100px"> {{$order->order_details}}</textarea>
                              <p> Phone : {{$order->phone}}</p>
                              <p> Weight : 
                                
                                
                                  

                         <?php
                              if(($order->weight_id()->get()) !== null){
                                  foreach($order->weight_id()->get() as $weight){
                                    echo $weight->name; 
                                  }   
                            }
                            ?>     
                                
                                
                                
                                
                             </p>
                                
                              
                                
                                
                            
                                
                                
                              <p> Time : {{$order->time}}</p>
                                
                              @if($order->is_fast == 1)
                                 <p style="color:#f00"> Fast Order</p>
                              @endif
                                
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
    &nbsp;
                    
                    
                    
                    
                    
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-danger"
                            data-toggle="modal" 
                            data-target="#delInvoice{{$invoice->id}}"
                            >
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="delInvoice{{$invoice->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Delete Invice</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p>
                                  Are You Sure To Delete 
                                       <?php
                                       if(($invoice->order_id()->get()) !== null){
                                          foreach($invoice->order_id()->get() as $order){
                                            echo $order->title; 
                                          }   
                                     }
                                    
                                    ?>
                                  
                                  
                                  ? 
                                
                                
                                </p>
                            </div>
                            <div class="modal-footer">
                                
                                   
                             {!! Form::open(['url' => 'del_invoice/'.$invoice->id , 'files' => true,'method'=>'delete']) !!}
                                
                             {!! Form::submit('Yes', ['class'=>' submit btn btn-primary'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
   
                    
                    
               
                    
                    
                </td>
     
            </tr>  
            
                
                @endforeach
            
                  </tbody>
    </table>
          
          
          
          
          
          
          
          
          
      </div>
		</div>


@include('template.includes.footer')