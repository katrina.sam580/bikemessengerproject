@include('template.includes.header')
		
		<div class="wrapper d-flex align-items-stretch">
	
            
            @include('template.includes.sidebar')
            

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

    @include('template.includes.navbar')
          
          
          
        <h2 class="mb-4">Transportations</h2>
        
          
                        
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-primary"
                            data-toggle="modal" 
                            data-target="#addTransport"
                            style="margin-bottom:30px;float:right"
                            >
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="addTransport" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Add Transportation</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p>
                                              
                             {!! Form::open(['url' => 'add_Transport']) !!}
                                
                                
                                 {!! Form::text('name' , old('name') ,
                                           ['class' => 'form-control' ,
                                                'placeholder' => 'Transport',
                                                'aria-required' => 'true',
                                                'autofocus' => 'true',
                                             
                                           ]) !!}
                               
                                </p>
                            </div>
                            <div class="modal-footer">
                                
                     
                                
                                
                             {!! Form::submit('Add', ['class'=>' submit btn btn-light'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
   
          
          
          
          
          
          
              
     <table id="allTransport" class="table table-striped table-bordered" style="width:100%;clear:both">
        <thead>
            <tr>
                <th>Transportation</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            
                 @foreach($transports as $transport)
            
        
            
            
            
            <tr>
                <td>
                    
                         {{$transport->name}}
                  
                </td>    
            
                
                
                
                
                
         
                <td>
               
                            
                    
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-danger"
                            data-toggle="modal" 
                            data-target="#delTransport{{$transport->id}}"
                            >
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="delTransport{{$transport->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Delete Transportation</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p>
                                  Are You Sure To Delete {{$transport->name}} ?
                              
                                
                                </p>
                            </div>
                            <div class="modal-footer">
                                
                                   
                             {!! Form::open(['url' => 'del_Transport/'.$transport->id , 'files' => true,'method'=>'delete']) !!}
                                
                             {!! Form::submit('Yes', ['class'=>' submit btn btn-primary'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
   
                     
          
               
                    &nbsp;
                    
                    
                    
                                  
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-success"
                            data-toggle="modal" 
                            data-target="#editTransport{{$transport->id}}"
                            >
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="editTransport{{$transport->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Edit Transportation</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                           
                                
                                {!! Form::open(['url' => 'edit_Transport/'.$transport->id]) !!}
                                
                                
                                 {!! Form::text('name' , $transport->name ,
                                           ['class' => 'form-control' ,
                                                'placeholder' => 'Transport',
                                                'aria-required' => 'true',
                                                'autofocus' => 'true',
                                             
                                           ]) !!}
                                
                                
                                
                                
                                
                            </div>
                            <div class="modal-footer">
                                
                                   
                         
                             {!! Form::submit('Edit', ['class'=>' submit btn btn-light'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
   
                     
                    
                    
                    
                    
                    
                    
                    
                </td>
     
            </tr>  
            
                
                @endforeach
            
                  </tbody>
    </table>
          
          
          
          
          
          
          
          
          
      </div>
		</div>


@include('template.includes.footer')