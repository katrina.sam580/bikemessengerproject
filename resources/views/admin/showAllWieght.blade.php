@include('template.includes.header')
		
		<div class="wrapper d-flex align-items-stretch">
	
            
            @include('template.includes.sidebar')
            

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

    @include('template.includes.navbar')
          
          
          
        <h2 class="mb-4">Wieghts</h2>
        
          
                        
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-primary"
                            data-toggle="modal" 
                            data-target="#addWieght"
                            style="margin-bottom:30px;float:right"
                            >
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="addWieght" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Add Wieght</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p>
                                              
                             {!! Form::open(['url' => 'weight']) !!}
                                
                                
                                 {!! Form::text('name' , old('name') ,
                                           ['class' => 'form-control' ,
                                                'placeholder' => 'Wieght',
                                                'aria-required' => 'true',
                                                'autofocus' => 'true',
                                             
                                           ]) !!}
                               
                                </p>
                            </div>
                            <div class="modal-footer">
                                
                     
                                
                                
                             {!! Form::submit('Add', ['class'=>' submit btn btn-light'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
   
          
          
          
          
          
          
              
     <table id="allWieghts" class="table table-striped table-bordered" style="width:100%;clear:both">
        <thead>
            <tr>
                <th>Wieght</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            
                 @foreach($weights as $weight)
            
        
            
            
            
            <tr>
                <td>
                    
                         {{$weight->name}}
                  
                </td>    
            
                
                
                
                
                
         
                <td>
               
                            
                    
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-danger"
                            data-toggle="modal" 
                            data-target="#delWieght{{$weight->id}}"
                            >
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="delWieght{{$weight->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Delete Wieght</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p>
                                  Are You Sure To Delete {{$weight->name}} ?
                              
                                
                                </p>
                            </div>
                            <div class="modal-footer">
                                
                                   
                             {!! Form::open(['url' => 'del_wieght/'.$weight->id , 'files' => true,'method'=>'delete']) !!}
                                
                             {!! Form::submit('Yes', ['class'=>' submit btn btn-primary'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
   
                     
          
               
                    &nbsp;
                    
                    
                    
                                  
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-success"
                            data-toggle="modal" 
                            data-target="#editWieght{{$weight->id}}"
                            >
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="editWieght{{$weight->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Edit Wieght</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                           
                                
                                {!! Form::open(['url' => 'edit_wieght/'.$weight->id]) !!}
                                
                                
                                 {!! Form::text('name' , $weight->name ,
                                           ['class' => 'form-control' ,
                                                'placeholder' => 'Wieght',
                                                'aria-required' => 'true',
                                                'autofocus' => 'true',
                                             
                                           ]) !!}
                                
                                
                                
                                
                                
                            </div>
                            <div class="modal-footer">
                                
                                   
                         
                             {!! Form::submit('Edit', ['class'=>' submit btn btn-light'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
   
                     
                    
                    
                    
                    
                    
                    
                    
                </td>
     
            </tr>  
            
                
                @endforeach
            
                  </tbody>
    </table>
          
          
          
          
          
          
          
          
          
      </div>
		</div>


@include('template.includes.footer')