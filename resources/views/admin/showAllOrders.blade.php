@include('template.includes.header')
		
		<div class="wrapper d-flex align-items-stretch">
	
            
            @include('template.includes.sidebar')
            

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

    @include('template.includes.navbar')
          
          
          
        <h2 class="mb-4">All Orders</h2>
        
          
                   
              
     <table id="allOrders" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Title</th>
                <th>User</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            
                 @foreach($orders as $order)
            
        
            
            
            
            <tr>
                <td>
                    
                         {{$order->title}}
                  
                </td>    
                <td>
                    
                                <?php
                                       if(($order->user_id()->get()) !== null){
                                          foreach($order->user_id()->get() as $user){
                                            echo $user->full_name; 
                                          }   
                                     }
                                    
                                    ?>
                  
                </td> 
                
                <td>
                  <p>
                             
                                     
                                       @if($order->status == 'is_confirm')
                                           <span style="color:#309b0d" >
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                            </span>
                                       @endif
                                    
                                     
                                       @if($order->status == 'is_not_confirm')
                                           <span style="color:red">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                           </span>
                                       @endif
                               
                                    
                                    
                                
                                </p>
                
                </td>
                
                
                
                
                
                
         
                <td>
               
                    
                    
                                <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-primary"
                            data-toggle="modal" 
                            data-target="#viewOrder{{$order->id}}"
                            >
                           <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="viewOrder{{$order->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4> {{$order->title}} </h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p> details :</p> <textarea readonly style="width:100%;min-height:100px"> {{$order->order_details}}</textarea>
                              <p> Phone : {{$order->phone}}</p>
                              <p> Weight : 
                                
                                
                                  

                         <?php
                              if(($order->weight_id()->get()) !== null){
                                  foreach($order->weight_id()->get() as $weight){
                                    echo $weight->name; 
                                  }   
                            }
                            ?>     
                                
                                
                                
                                
                             </p>
                                
                              
                                
                                
                            
                                
                                
                              <p> Time : {{$order->time}}</p>
                                
                              @if($order->is_fast == 1)
                                 <p style="color:#f00"> Fast Order</p>
                              @endif
                                
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
    
               
                    
                    
                </td>
     
            </tr>  
            
                
                @endforeach
            
                  </tbody>
    </table>
          
          
          
          
          
          
          
          
          
      </div>
		</div>


@include('template.includes.footer')