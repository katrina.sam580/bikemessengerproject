	<nav id="sidebar" class="active">
				<h1><a href="index.html" class="logo">M.</a></h1>
                
        <ul class="list-unstyled components mb-5">
            
            
            <!--Start SideBar User-->
            @if(auth()->user() && auth()->user()->user_type == 'is_user')
            
          <li class="active">
            <a href="{{url('/')}}"><span class="fa fa-home"></span> Home</a>
          </li>
          <li>
             <a href="{{url('order/create')}}"><span class="fa fa-newspaper-o"></span>Request Order</a>

          </li>  
          <li>
             <a href="{{url('pending/orders')}}"><span class="fa fa-newspaper-o"></span>Pending Order</a>

          </li>
          <li>
            <a href="{{url('previous/orders')}}"><span class="fa fa-sticky-note"></span> Prev Order</a>
          </li>
          <li>
            <a href="#"><span class="fa fa-bell"></span> Notifications</a>
          </li>
          <li>
            <a href="{{url('fines')}}"><span class="fa fa-money"></span> Fines</a>
          </li>
          <li>
            <a href="#"><span class="fa fa-paper-plane"></span> Offers</a>
          </li>
          <li>
            <a href="invoices"><span class="fa fa-file-text-o"></span> Invoices</a>
          </li>
          <li>
            <a href="#"><span class="fa fa-gear"></span> Settings</a>
          </li>
          <li>
            <a href="#"><span class="fa fa-user"></span> My Profile</a>
          </li>
            @endif
            
                 <!--End SideBar User-->
            
            
            
            
                <!--Start SideBar Driver-->
            
             @if(auth()->user() && auth()->user()->user_type == 'is_driver')

             <li>
              <a href="{{url('all/pending/orders')}}"><span class="fa fa-newspaper-o"></span>Pending Order</a>

            </li>
            
             @endif
            
                <!--End SideBar Driver-->
            
            
            
            
            
             
            
                <!--Start SideBar Admin-->
            
             @if(auth()->user() && auth()->user()->user_type == 'is_admin')

             <li>
                 <a href="{{url('all/clients')}}"><span class="fa fa-users"></span>Clients</a>
             </li>
             <li>
                 <a href="{{url('all/drivers')}}"><span class="fa fa-bicycle"></span>Drivers</a>
             </li>
              <li>
                 <a href="{{url('all/orders')}}"><span class="fa fa-newspaper-o"></span>Orders</a>
             </li>
             <li>
                 <a href="{{url('all/invoices')}}"><span class="fa fa-file-text-o"></span>Invoices</a>
             </li>
             <li>
                 <a href="{{url('all/fines')}}"><span class="fa fa-money"></span>Fines</a>
             </li>
             <li>
                 <a href="{{url('all/wieght')}}"><span class="fa fa-cubes"></span>Wieghts</a>
             </li>
             <li>
                 <a href="{{url('all/transports')}}"><span class="fa fa-car"></span>Transportation</a>
             </li>
             <li>
                 <a href="{{url('')}}"><span class="fa fa-gear"></span>Services</a>
             </li>
        
            
             @endif
            
                <!--End SideBar Admin-->


            
        </ul>

        <div class="footer">
        	<p>
					  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib.com</a>
					</p>
        </div>
    	</nav>