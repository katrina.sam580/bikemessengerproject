<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
   <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
        
        
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                   <!--     @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif-->
                    @endauth
                </div>
            @endif

            <div style="background: rgba(0,0,0,0.1);padding: 25px;height:250px;border-radius: 20px;">
                <h4 style="margin-bottom:30px">Please Choose Type Of Your Account :</h4>
            
                <div class="row" style="margin:auto;width:225px">
                   <button class="btn btn-primary col-md-12" style="margin-bottom:10px">
                    <a href="{{url('user')}}" style="color:#fff;text-decoration:none">User</a>
                    </button><br/>
                </div>
                <div class="row" style="margin:auto;width:225px">
                   <button class="btn btn-primary col-md-12" style="margin-bottom:10px">
                      <a href="{{url('driver')}}" style="color:#fff;text-decoration:none">Driver</a>
                    </button><br/>
                </div>
            </div>
        </div>
    </body>
</html>
