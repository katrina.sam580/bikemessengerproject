@include('template.includes.header')
		
		<div class="wrapper d-flex align-items-stretch">
	
            
            @include('template.includes.sidebar')
            

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

    @include('template.includes.navbar')
          
          
          
        <h2 class="mb-4">Invoices</h2>
        
          
                   
              
     <table id="allInvoices" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Title</th>
                <th>Invoice Date</th>
                <th>Total Cost</th>
                <th>Total Fines</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            
                 @foreach($invoices as $invoice)
            
        
            
            
            
            <tr>
            
                <td>
                    {{$invoice->title}}
                  
                </td> 
                
                <td>
                
                      {{$invoice->invoice_date}}
                        
                                     
                
                </td>   
                <td>
               
                      {{$invoice->total_cost}}
                              
                
                </td>  
                
                <td>
              
                      {{$invoice->total_fines}}
                               
                
                </td>
                
                
                
                
                
                
         
                <td>
               
                    
                    
                                <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-primary"
                            data-toggle="modal" 
                            data-target="#viewOrder{{$invoice->id}}"
                            >
                           <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="viewOrder{{$invoice->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4> {{$invoice->title}} </h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p> details :</p> <textarea readonly style="width:100%;min-height:100px"> {{$invoice->order_details}}</textarea>
                              <p> Phone : {{$invoice->phone}}</p>
                              <p> Weight : 
                                
                                
                                  

                         <?php
                                  
                            $wieght = App\orders::where('id',$invoice->order_id)->first();
                              if(( $wieght->weight_id()->get()) !== null){
                                  foreach( $wieght->weight_id()->get() as $weightName){
                                    echo $weightName->name; 
                                  }   
                            }
                            ?>     
                                
                                
                                
                                
                             </p>
                                
                              
                                
                                
                            
                                
                                
                              <p> Time : {{$invoice->time}}</p>
                                
                              @if($invoice->is_fast == 1)
                                 <p style="color:#f00"> Fast Order</p>
                              @endif
                                
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
               
                    
               
                    
                    
                </td>
     
            </tr>  
            
                
                @endforeach
            
                  </tbody>
    </table>
          
          
          
          
          
          
          
          
          
      </div>
		</div>


@include('template.includes.footer')