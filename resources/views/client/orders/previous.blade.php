@include('template.includes.header')
		
		<div class="wrapper d-flex align-items-stretch">
	
            
            @include('template.includes.sidebar')
            

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

    @include('template.includes.navbar')
          
          
          
        <h2 class="mb-4">Previous Request</h2>
        
          
                   
              
     <table id="prevOrders" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Title</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            
                 @foreach($prevOrders as $order)
            
        
            
            
            
            <tr>
                <td>
                    
                         {{$order->title}}
                  
                </td> 
         
                <td>
               
                    
                    
                                <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-primary"
                            data-toggle="modal" 
                            data-target="#viewOrder{{$order->id}}"
                            >
                           <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="viewOrder{{$order->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4> {{$order->title}} </h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p> details :</p> <textarea readonly style="width:100%;min-height:100px"> {{$order->order_details}}</textarea>
                              <p> Phone : {{$order->phone}}</p>
                              <p> Weight : 
                                
                                
                                  

                         <?php
                              if(($order->weight_id()->get()) !== null){
                                  foreach($order->weight_id()->get() as $weight){
                                    echo $weight->name; 
                                  }   
                            }
                            ?>     
                                
                                
                                
                                
                             </p>
                              <p> Time : {{$order->time}}</p>
                                
                              @if($order->is_fast == 1)
                                 <p style="color:#f00"> Fast Order</p>
                              @endif
                                
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
    
                    
                    &nbsp;
                    
                    
             
                   <button class="btn btn-sm btn-info"> 
                      <a href="{{url('reSendOrder/'.$order->id)}}" style="color:#fff">  <i class="fa fa-send" aria-hidden="true"></i> </a>
                   </button> 
                    
                            
                    
                    
                </td>
     
            </tr>  
            
                
                @endforeach
            
                  </tbody>
    </table>
          
          
          
          
          
          
          
          
          
      </div>
		</div>


@include('template.includes.footer')