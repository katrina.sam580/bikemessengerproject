@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Request Order') }}</div>

                <div class="card-body">
                
                    <?php
                        $id = auth()->user()->id;
                    ?>
                   
                    
                     {!! Form::open(['url' => 'order/'.$id , 'files' => true]) !!}

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    
                    
                        <div class="form-group row">
                            <label for="order_details" class="col-md-4 col-form-label text-md-right">{{ __('Order Details') }}</label>

                            <div class="col-md-6">
                       
                                <textarea id="order_details" class="form-control @error('order_details') is-invalid @enderror" name="order_details" value="{{ old('order_details') }}" required></textarea>
                                
                                @error('order_details')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    
                    
                        
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>  
                        
                                            
          
                        
                       <div class="form-group row">
                            <label  class="col-md-4 col-form-label text-md-right">{{ __('Weight') }}</label>

                            <div class="col-md-6">

                                
                                         {!! Form::select('weight_id' ,App\weights::pluck('name' , 'id'), old('weight_id') ,
                                                           ['class' => 'form-control',
                                                                'placeholder' => 'Weight  ',
                                                                'required' => 'true',
                                                                'style' => 'margin-bottom:20px;margin-top:10px ',


                                          ]) !!}
                                                                                
                                
                                
                            </div>
                        </div>
                    
                    
                    
                    
                       <div class="form-group row">
                            <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>

                            <div class="col-md-6">
                                <input id="time" type="text" class="form-control @error('time') is-invalid @enderror" name="time" value="{{ old('time') }}" required autocomplete="time" autofocus>

                                @error('time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                    
                    
                        <div class="form-group row">
                           <center>
                               <label for="is_fast" >{{ __('Is Fast') }}</label>

                              
                                     {{ Form::checkbox('is_fast') }}
                         </center>
                              
                        </div>
                        
                 
                    
                        
                        <hr/>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                           
                               <center>
                                
                                 {!! Form::submit('Request', ['class'=>' submit btn btn-primary' ]) !!} 
                                <button class="btn btn-primary"><a href="{{url('Home/User')}}" style="color:#fff;text-decoration:none">Back</a></button>
                               </center>
                                
                            </div>
                        </div>
                        
                                  
                        
                     
                        
                           

                            {!! Form::close() !!}
                    
                    
               
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
