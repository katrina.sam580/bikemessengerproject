@include('template.includes.header')
		
		<div class="wrapper d-flex align-items-stretch">
	
            
            @include('template.includes.sidebar')
            

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

    @include('template.includes.navbar')
          
          
          
        <h2 class="mb-4">Pending Request</h2>
        
          
                   
              
              <table id="pendingOrders" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Title</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            
                 @foreach($pendingOrders as $order)
            
        
            
            
            
            <tr>
                <td>
                    
                         {{$order->title}}
                  
                </td> 
         
                <td>
              
                  
                    
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-primary"
                            data-toggle="modal" 
                            data-target="#viewOrder{{$order->id}}"
                            >
                           <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="viewOrder{{$order->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4> {{$order->title}} </h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p> details :</p> <textarea readonly style="width:100%;min-height:100px"> {{$order->order_details}}</textarea>
                              <p> Phone : {{$order->phone}}</p>
                              <p> Weight : 
                                
                                
                                  

                         <?php
                              if(($order->weight_id()->get()) !== null){
                                  foreach($order->weight_id()->get() as $weight){
                                    echo $weight->name; 
                                  }   
                            }
                            ?>     
                                
                                
                                
                                
                             </p>
                              <p> Time : {{$order->time}}</p>
                                
                              @if($order->is_fast == 1)
                                 <p style="color:#f00"> Fast Order</p>
                              @endif
                                
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
    
                    
                    &nbsp;
    
                    
                     
                 
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-success"
                            data-toggle="modal" 
                            data-target="#editPenOrder{{$order->id}}"
                            >
                             <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                      <div class="modal fade" id="editPenOrder{{$order->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4> Edit {{$order->title}} </h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                         
                                  {!! Form::open(['url' => 'editPendingOrder/'.$order->id , 'files' => true,'method'=>'post']) !!}
                               
                                
                                
                               <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{$order->title}}" required autocomplete="title" autofocus>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                                
                               
                    
                    
                        <div class="form-group row">
                            <label for="order_details" class="col-md-4 col-form-label text-md-right">{{ __('Order Details') }}</label>

                            <div class="col-md-6">
                       
                                <textarea id="order_details" class="form-control @error('order_details') is-invalid @enderror" name="order_details"  required style="width:100%;min-height:100px"> {{$order->order_details}}</textarea>
                                
                                @error('order_details')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    
                                
                                
                                
                             
                        
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{$order->phone}}" required autocomplete="phone" autofocus>

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>  
                        
                                            
          
                        
                       <div class="form-group row">
                            <label  class="col-md-4 col-form-label text-md-right">{{ __('Weight') }}</label>

                            <div class="col-md-6">

                                
                                         {!! Form::select('weight_id' ,App\weights::pluck('name' , 'id'),$order->weight_id ,
                                                           ['class' => 'form-control',
                                                                'placeholder' => 'Weight  ',
                                                                'required' => 'true',
                                                                'style' => 'margin-bottom:20px;margin-top:10px ',


                                          ]) !!}
                                                                                
                                
                                
                            </div>
                        </div>
                    
                    
                    @if($order->time)
                                
               
                    
                       <div class="form-group row">
                            <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>

                            <div class="col-md-6">
                                <input id="time" type="text" class="form-control @error('time') is-invalid @enderror" name="time" value="{{$order->time}}" required autocomplete="time" autofocus>

                                @error('time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                                
                     @elseif(!$order->time)
                                     <div class="form-group row">
                            <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>

                            <div class="col-md-6">
                                <input id="time" type="text" class="form-control @error('time') is-invalid @enderror" name="time" value="{{old('time')}}" required autocomplete="time" autofocus>

                                @error('time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                               
                                
                                
                     @endif
                    
                                
                                
                    @if($order->is_fast)
                        <div class="form-group row">
                           <center>
                               <label for="is_fast" >{{ __('Is Fast') }}</label>

                              
                                     {{ Form::checkbox('is_fast',null,true) }}
                         </center>
                              
                        </div>
                                
                     @elseif(!$order->is_fast)
                        <div class="form-group row">
                           <center>
                               <label for="is_fast" >{{ __('Is Fast') }}</label>

                              
                                     {{ Form::checkbox('is_fast') }}
                          </center>
                              
                        </div>       
                                
                                
                     @endif
                        
                        
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                            </div>
                            <div class="modal-footer">
                                
                                
                                
                                   {!! Form::submit('Edit', ['class'=>' submit btn btn-success' ]) !!} 

                                    {!! Form::close() !!}
                                     
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
                                  
                    
                    
                    
                    
                    &nbsp;
            
                    
                      <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-sm btn-danger"
                            data-toggle="modal" 
                            data-target="#delPenOrder{{$order->id}}"
                            >
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                    
                    
                    
                     <!-- Modal -->
                    <div class="modal fade" id="delPenOrder{{$order->id}}" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                                <h4>Delete Order</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                             
                            </div>
                            <div class="modal-body">
                              <p>Are You Sure To Delete " {{$order->title}}  " Order ? </p>
                            </div>
                            <div class="modal-footer">
                                
                                   
                             {!! Form::open(['url' => 'del_pending_order/'.$order->id , 'files' => true,'method'=>'delete']) !!}
                                
                             {!! Form::submit('Yes', ['class'=>' submit btn btn-primary'
                                                                                  ]) !!}


                             {!! Form::close() !!}
                                
                                
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                </td>
     
            </tr>  
            
                
                @endforeach
            
                  </tbody>
    </table>
          
          
          
          
          
          
          
          
          
      </div>
		</div>


@include('template.includes.footer')