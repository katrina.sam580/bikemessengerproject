<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Login Api
// http://127.0.0.1:8000/oauth/token




/////////////////////////////////////////// Client ///////////////////////////////////////

 // Middleware Client
 Route::group(['middleware' =>  ['auth:api' ,'client:api']  ] , function (){



    Route::post('edit/account/user/{id}','API\Orders\ordersController@editUserAccount');

    //show Pending Orders
    Route::get('pending/orders','API\Orders\ordersController@showPendingOrders'); //http://127.0.0.1:8000/api/pending/orders


    //View Request Order
    Route::get('order/create','API\Orders\ordersController@create'); //http://127.0.0.1:8000/api/order/create



    //Request Order
    Route::post('order','API\Orders\ordersController@store'); // http://127.0.0.1:8000/api/order



     //ReSend Order
     Route::post('reSendOrder/{id}','API\Orders\ordersController@reSendOrder'); //http://127.0.0.1:8000/api/reSendOrder/id


    //Edit Pending Order
    Route::post('editPendingOrder/{id}','API\Orders\ordersController@editPendingOrder'); // http://127.0.0.1:8000/api/editPendingOrder/id


     //Cancle Pending Order
     Route::get('cancle/order/{id}','API\Orders\ordersController@cancleOrder');//http://127.0.0.1:8000/api/cancle/order/id


    //Show Fines (Client)
    Route::get('fines','API\Fines\finesController@showFines'); // http://127.0.0.1:8000/api/fines


    //Show invoices (Client)
    Route::get('invoices','API\Invoices\invoicesController@showInvoices');//http://127.0.0.1:8000/api/invoices



    //Previous Orders
    Route::get('previous/orders','API\Orders\ordersController@showPreviousOrders');// http://127.0.0.1:8000/api/previous/orders



    //Delete Order
    Route::delete('order/{id}','API\Orders\ordersController@destroy');// http://127.0.0.1:8000/api/order/id

     // Weights
     Route::get('weight','API\Weights\weightsController@index');
     //http://127.0.0.1:8000/api/weight {show} GET


 });




 /////////////////////////////////////////// Driver ///////////////////////////////////////

 // Middleware Driver
 Route::group(['middleware' => ['auth:api' ,'driver:api']  ] , function (){


     Route::post('edit/account/driver/{id}','API\Orders\ordersController@editDriverAccount');
    //Get All Pending Request (Driver)
    Route::get('all/pending/orders','API\Orders\ordersController@allPendingOrder');// http://127.0.0.1:8000/api/all/pending/orders


    //Confirm Pending Request (Driver)
    Route::post('confirm_order/{id}','API\Orders\ordersController@confirmOrder');// http://127.0.0.1:8000/api/confirm_order/id


    //Show Confirmed Order (Driver)
    Route::get('all/confirmed/order','API\Orders\ordersController@showConfirmedOrderForDeriver');// http://127.0.0.1:8000/api/all/confirmed/order


    //Show Detils Of Invoice (Delivery Order)
    Route::get('delivery/order/{id}','API\Orders\ordersController@deliveryOrder');// http://127.0.0.1:8000/api/delivery/order/id



     //Send Invoice To Client
    Route::get('send/invoice/{id}','API\Invoices\invoicesController@sendInvoiceToClient');// http://127.0.0.1:8000/api/send/invoice/id

     Route::post('create/invoice','API\Invoices\invoicesController@createinvoice');

    Route::post('edit/current_location','API\Orders\ordersController@editLatLangDriver');// http://127.0.0.1:8000/api/edit/current_location
 });






/////////////////////////////////////////// Admin ///////////////////////////////////////

 // Middleware Admin
 Route::group(['middleware' => ['auth:api' ,'admin:api'] ] , function (){

    //Show All Clients
    Route::get('all/clients','API\dashboardController@viewAllClients'); // http://127.0.0.1:8000/api/all/clients




   //Show All Drivers
   Route::get('all/drivers','API\dashboardController@viewAllDrivers'); //http://127.0.0.1:8000/api/all/drivers


  //Delete Client
  Route::delete('del_client/{id}','API\dashboardController@delClient');//http://127.0.0.1:8000/api/del_client/id




 //Accept Driver
 Route::post('accept_driver/{id}','API\dashboardController@acceptDriver');//http://127.0.0.1:8000/api/accept_driver/id



 //Show All Orders
 Route::get('all/orders','API\dashboardController@viewAllOrders');//http://127.0.0.1:8000/api/all/orders



    //Show All Fines (Admin)
    Route::get('all/fines','API\Fines\finesController@viewAllFines');// http://127.0.0.1:8000/api/all/fines



     //Show All Invoices (Admin)
     Route::get('all/invoices','API\Invoices\invoicesController@viewAllInvoices');//http://127.0.0.1:8000/api/all/invoices

     //Delete Invoice
     Route::delete('del_invoice/{id}','API\Invoices\invoicesController@delInvoice');//http://127.0.0.1:8000/api/del_invoice/id




     // Weights
    Route::resource('weight','API\Weights\weightsController');
    //http://127.0.0.1:8000/api/weight {add}  POST
    //http://127.0.0.1:8000/api/weight/{id} {edit} PUT
    //http://127.0.0.1:8000/api/weight {show} GET
    //http://127.0.0.1:8000/api/weight {show}  DELETE





    // Transporant
    Route::resource('transporant','API\Transporants\transportsController');
    //http://127.0.0.1:8000/api/transporant {add}  POST
    //http://127.0.0.1:8000/api/transporant/{id} {edit} PUT
    //http://127.0.0.1:8000/api/transporant {show} GET
    //http://127.0.0.1:8000/api/transporant {show}   DELETE



 });

/////////////////////////Register/////////////////////////////

//Register User
Route::post('register/user','API\Auth\RegisterController@registerUser');   //http://localhost:8000/api/register/user

//Register Driver
Route::post('register/driver','API\Auth\RegisterController@registerDriver'); //http://localhost:8000/api/register/driver


//Logout
Route::group(['middleware' => 'auth:api'] , function () {
    Route::get('logout', 'API\Auth\RegisterController@logout');
});


/// /////////////////////////////////////////////////////////

//middleware Auth
Route::group(['middleware' => 'auth:api'] , function (){

    Route::get('hello',function (){
        echo "test";
    }) ;

});



























