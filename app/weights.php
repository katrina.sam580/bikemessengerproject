<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class weights extends Model
{
    protected $fillable = ['name','price'];
}
