<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orders extends Model
{
    protected $fillable = ['title','order_details','phone','time','date','user_id','weight_id','invoice_id','status','is_fast','is_not_confirm','is_not_cancle','driver_id','lng','lat'];
//     protected $casts = ['time' => 'H:i A','date' => 'd-m-Y'];
      public function weight_id(){
            return $this->hasOne('App\weights' , 'id' , 'weight_id');
      }

      public function user_id(){
            return $this->hasOne('App\User' , 'id' , 'user_id');
      }

}
