<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fines extends Model
{
    protected $fillable = ['order_id','fine_value','is_payment'];

        
    public function order_id(){
            return $this->hasOne('App\orders' , 'id' , 'order_id');
      }  
    
}
