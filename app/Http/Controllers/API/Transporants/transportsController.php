<?php

namespace App\Http\Controllers\API\Transporants;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\transportations;


use App\Http\Controllers\API\BaseController as BaseController;


class transportsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    //Show All Transports
    public function index()
    {
         $transports = transportations::get();
       // dd($weights);
        
        if(!$transports->toArray()){
           return $this->sendResponse($transports->toArray(), 'No Transportations Yet.');
        }else{
           return $this->sendResponse($transports->toArray(), 'Show Transportations successfully.');
        }
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    //Add Transport
    public function store(Request $request)
    {
        $data = Validator::make(request()->all(), [
            'name'=>'required|string|max:255',
        ]);
        
           if($data->fails()){
            return $this->sendError('Validation Error.', $data->errors());       
        }

        
        $transports = transportations::create([
           'name' => request()->name,
    
        ]);
        
        return $this->sendResponse($transports, 'Add Transportation successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     //Edit Transport
    public function update(Request $request, $id)
    {
         $data = Validator::make(request()->all(), [
                  'name'=>'required|string|max:255',
          ]); 
        
          if($data->fails()){
            return $this->sendError('Validation Error.', $data->errors());       
        }
        
         transportations::where('id',$id)->update([
           'name' => request()->name,
        ]);  
        
         $transports = transportations::where('id',$id)->first(); 
        
        return $this->sendResponse($transports, 'Update Transportation successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transport =  transportations::where('id',$id)->first();
        transportations::find($id)->delete();
        return $this->sendResponse($transport, 'Update Transportation successfully.');
    }
}
