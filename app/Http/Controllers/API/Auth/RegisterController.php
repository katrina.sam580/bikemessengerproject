<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\BaseController as BaseController;
class RegisterController extends BaseController
{
    //
    protected $redirectTo = '/home';
    private $token;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function registerUser(Request $request){

        $data = Validator::make($request->all(), [
            'full_name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255|unique:users',
            'password'=>'required|string|min:8|confirmed',
            'phone'=>'required|numeric',
//            'photo'=>'image',

        ]);

        if($data->fails())
        {
            return $this->sendError('Validation Error.', $data->errors());
        }

        $timeFolder = time();
        $user =  User::create([
            'full_name' => $request->full_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'is_accepted' => 1,
            'user_type' => 2,
        ]);



        if(request('photo')){
            User::where('id',$user->id)->update([
                'photo' => $request->file("photo")->store('photo/profile/'.$user->id),
            ]);
        }

        Auth::login($user);

        $access_token=$user->createToken('authToken')->accessToken; //to make user auth
        $this->token=$access_token;



        return $this->sendResponseWithToken($user->toArray(),$access_token,'user register successfully.');



    }






    public function registerDriver(Request $request){
        $data = Validator::make($request->all(), [
            'full_name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255|unique:users',
            'password'=>'required|string|min:8|confirmed',
            'phone'=>'required|numeric',
            'photo'=>'image',
            'front_identification_photo'=>'required|image',
            'back_identification_photo'=>'required|image',
            'transportation_id'=>'required',
            'current_lng'=>'required',
            'current_lat'=>'required',

        ]);

        if($data->fails())
        {
            return $this->sendError('Validation Error.', $data->errors());
        }

        $timeFolder = time();
        $user =  User::create([
            'full_name' => $request->full_name,
            'email' =>$request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,

            'front_identification_photo' => $request->file("front_identification_photo")->store('image_front_'.$timeFolder),
            'back_identification_photo' => $request->file("back_identification_photo")->store('image_back_'.$timeFolder),
            'transportation_id' => $request->transportation_id,
            'is_accepted' => 0,
            'user_type' => 1,
            'current_lng' => $request->current_lng,
            'current_lat' => $request->current_lat,
        ]);

//        //if(request()->file("photo")){
//            User::where('id',$user->id)->update([
//                'photo' => $request->file("photo")->store('photo/profile/'.$user->id),
//            ]);
//        //}
        if($request->photo) {
            DB::table('users')->where('id', $user->id)->update([
                'photo' => $request->file("photo")->store('photo/profile/'.$user->id),
            ]);
         }


        //To Rename Front Image
        $newNameFrontImg = str_replace('image_front_'.$timeFolder , 'photo/frontIdent/'.$user->id, $user['front_identification_photo']);
        \Storage::rename($user['front_identification_photo'] , $newNameFrontImg);
        User::where('id' , $user->id )->update(['front_identification_photo'=>$newNameFrontImg]);
        \Storage::deleteDirectory('image_front_'.$timeFolder);


        //To Rename Back Image
        $newNameBackImg = str_replace('image_back_'.$timeFolder , 'photo/backIdent/'.$user->id, $user['back_identification_photo']);
        \Storage::rename($user['back_identification_photo'] , $newNameBackImg);
        User::where('id' , $user->id )->update(['back_identification_photo'=>$newNameBackImg]);
        \Storage::deleteDirectory('image_back_'.$timeFolder);

        Auth::login($user);

        $access_token=$user->createToken('authToken')->accessToken; //to make driver auth
        $this->token=$access_token;



        return $this->sendResponseWithToken($user->toArray(),$access_token,'driver register successfully.');

//
//        return redirect('/');
    }

    public function logout(){

        DB::table('oauth_access_tokens')
            ->where('user_id', '=',Auth::user()->id)
            ->update([
                'revoked' => true
            ]);

        return $this->sendResponse([],'Successfully logged out');

    }

}
