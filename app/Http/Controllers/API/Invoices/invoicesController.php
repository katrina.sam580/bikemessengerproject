<?php

namespace App\Http\Controllers\API\Invoices;


use Illuminate\Http\Request;

use App\Http\Controllers\API\BaseController as BaseController;

use App\invoices;
use App\fines;
use App\orders;
use Illuminate\Support\Facades\Validator;

class invoicesController extends BaseController
{

     /////////////////////// Client Function ////////////////////


    //Show Invoices
    public function showInvoices(){

         $invoices = \DB::table('orders')
            ->join('invoices', 'orders.id', '=', 'invoices.order_id')
            ->where(['user_id'=>auth()->user()->id,'is_send'=>1])
            ->get();

         //dd($invoices);


        if(!$invoices->toArray()){
           return $this->sendResponse($invoices->toArray(), 'No Invoices Yet.');
        }else{
           return $this->sendResponse($invoices->toArray(), 'Show Invoices successfully.');
        }


    }



    /////////////////////// Admin Function ////////////////////



    //Show All Invoices (Admin)
    public function viewAllInvoices(){
        $invoices = invoices::get();
       // dd($invoices);

        if(!$invoices->toArray()){
           return $this->sendResponse($invoices->toArray(), 'No Invoices Yet.');
        }else{
           return $this->sendResponse($invoices->toArray(), 'Show Invoices successfully.');
        }

    }



    //Delete Invoice
    public function delInvoice($id){
        $invoice = invoices::where('id',$id)->first();
        invoices::find($id)->delete();
       return $this->sendResponse($invoice, ' Delete Invoice successfully.');
    }




    //////////////////////////////////// Driver ////////////////////////////////
    public function sendInvoiceToClient($id){

         $invoice = invoices::where('id',$id)->first();

         $order = orders::where('id',$invoice->order_id)->first();

        //To Payment The Fines
          \DB::table('orders')
            ->join('fines', 'orders.id', '=', 'fines.order_id')
            ->where(['orders.user_id'=>$order->user_id,'fines.is_payment'=>0])
            ->update([
               'is_payment'=>1
            ]);


         invoices::where('id',$id)->update([
          'is_send'=> 1,
         ]);

         $invoice = invoices::where('id',$id)->first();

        return $this->sendResponse($invoice, ' Send Invoice successfully.');
    }



    public function createinvoice()
    {
        //'total_cost','total_fines','order_id','is_send'
        $data = Validator::make(request()->all(), [
          'total_cost' => 'required',
          'order_id' => 'required',
          'order_title' => 'required'
        ]);

        if($data->fails()){
            return $this->sendError('Validation Error.', $data->errors());
        }

        $invoice = invoices::create([
            'total_cost' => \request()->total_cost,
            'total_fines' => \request()->total_fines,
            'order_id' => \request()->order_id,
            'order_title' => \request()->order_title,
        ]);


        $invoice =  invoices::where('id',$invoice->id)->first();
        //return back();

        return $this->sendResponse($invoice->toArray(), 'invoice Created successfully.');
    }







}
