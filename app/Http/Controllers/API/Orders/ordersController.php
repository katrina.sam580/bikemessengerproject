<?php

namespace App\Http\Controllers\API\Orders;


use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\orders;
use App\fines;
use App\invoices;

use App\Http\Controllers\API\BaseController as BaseController;

class ordersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('client.orders.create');
        $data = "no thing";
        return $this->sendResponse($data, 'View Request Order successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $current_date=\Carbon\Carbon::now()->format('d-m-Y');
//        $current_time=\Carbon\Carbon::now()->format('H:i');

      $data = Validator::make(request()->all(), [

            'title'=>'required|string|max:255',
            'order_details'=>'required|string',
            'phone'=>'required|numeric',
            'weight_id'=>'required',
            'date'=>"after_or_equal:$current_date",
            'time'=>'date_format:H:i',
            'status'=>'string',
            'is_fast'=>'boolean',
             'lng'=>'required',
             'lat'=>'required'
        ]);

        if($data->fails()){
            return $this->sendError('Validation Error.', $data->errors());
        }



         $order = orders::create([
           'title' => request()->title,
           'order_details' => request()->order_details,
           'phone' => request()->phone,
           'weight_id' => request()->weight_id,
           'user_id' => auth()->user()->id,
           'lng'=>request()->lng,
           'lat'=>request()->lat,
           'status' => 2,
        ]);




        //date
        if(request('date')){
            orders::where('id',$order->id)->update([
                 'date' => request('date')
            ]);
        }

        //time
        if(request('time')){
            orders::where('id',$order->id)->update([
                'time' => request('time')
            ]);
        }



        if(request()->is_fast){
            orders::where('id',$order->id)->update([
                 'is_fast' => 1
            ]);
        }


        $orders =  orders::where('id',$order->id)->first();
        //return back();

        return $this->sendResponse($orders->toArray(), 'order Strored successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $order = orders::where('id',$id)->first();
       orders::find($id)->delete();

       return $this->sendResponse($order, 'Order Deleted successfully.');
    }







    //////////////////////////// My Function Added ////////////////////////////

    //Show Pending Orders
    public function showPendingOrders(){
       $pendingOrders = orders::where([
            'user_id' => auth()->user()->id,
            'status' => 'is_not_confirm',
        ])->get();


        if(!$pendingOrders->toArray()){
           return $this->sendResponse($pendingOrders->toArray(), 'No Pending Orders Yet.');
        }else{
           return $this->sendResponse($pendingOrders->toArray(), 'Show Pending Orders successfully.');
        }
    }



    //Cancle Order
    public function cancleOrder($id){

        $order = orders::where('id',$id)->update([
           'status' => 'is_cancle'
        ]);

       $fines =  fines::create([
            'order_id'=>$id,
            'fine_value'=>500,
            'is_payment'=>0,
        ]);


        $fine = fines::where('id',$fines->id)->first();

        return $this->sendResponse($fine->toArray(), 'Order Cancled successfully.');
    }


    //Edit Pending Order
    public function editPendingOrder($id){

        $current_date=\Carbon\Carbon::now()->format('d-m-Y');
//        $current_time=\Carbon\Carbon::now()->format('g:i A');

           $data = Validator::make(request()->all(), [
            'title'=>'required|string|max:255',
            'order_details'=>'required|string',
            'phone'=>'required|numeric',
            'weight_id'=>'required',
            'date'=>"after_or_equal:$current_date",
            'time'=>'date_format:H:i',
            'status'=>'string',
            'is_fast'=>'boolean',
               'lng'=>'required',
               'lat'=>'required'
        ]);



        if($data->fails()){
            return $this->sendError('Validation Error.', $data->errors());
        }


         $order = orders::where('id',$id)->update([
           'title' => request()->title,
           'order_details' => request()->order_details,
           'phone' => request()->phone,
           'weight_id' => request()->weight_id,
        ]);


        //time
        if(request('time')){
            $order = orders::where('id',$id)->update([
                 'time' => request('time')
            ]);
        }

        //date
        if(request('date')){
            $order = orders::where('id',$id)->update([
                'time' => request('date')
            ]);
        }

       // dd(request()->is_fast);
        if(request()->is_fast){
            $order = orders::where('id',$id)->update([
                 'is_fast' => 1
            ]);
        }

       if(request()->is_fast == null){
            $order =orders::where('id',$id)->update([
                 'is_fast' => 0
            ]);
        }


        $order =orders::where('id',$id)->get();

       // return back();
        return $this->sendResponse($order, 'pending order Updated successfully.');

    }


    //Show Prev Orders
    public function showPreviousOrders(){
       $prevOrders = orders::where([
            'user_id' => auth()->user()->id,
            'status' => 'is_confirm',
        ])->get();


       if(!$prevOrders->toArray()){
           return $this->sendResponse($prevOrders, 'No Previous Order Yet.');
        }else{
           return $this->sendResponse($prevOrders, 'Show Previous Order successfully.');
        }


    }



    //ReSend Order
    public function reSendOrder($id)
    {
        $prevOrder = orders::where('id', $id)->first();
        //dd($order->title);
        $current_date=\Carbon\Carbon::now()->format('d-m-Y');

        $data = Validator::make(request()->all(), [
            'date'=>"after_or_equal:$current_date",
            'time'=>'date_format:H:i',
        ]);

        if($data->fails()){
            return $this->sendError('Validation Error.', $data->errors());
          }

        $newOrder = orders::create([
            'title' => $prevOrder->title,
            'order_details' => $prevOrder->order_details,
            'phone' => $prevOrder->phone,
            'weight_id' => $prevOrder->weight_id,
            'user_id' => $prevOrder->user_id,
            'status' => 2,
            'lng'=>$prevOrder->lng,
            'lat'=>$prevOrder->lat,
        ]);


//        if ($prevOrder->time) {
//            orders::where('id', $newOrder->id)->update([
////                'time' => $prevOrder->time
//                'time' => \request('time')
//            ]);
//        }

        if (\request('time')&&$prevOrder->time) {
            orders::where('id', $newOrder->id)->update([
//                'time' => $prevOrder->time
                'time' => \request('time')
            ]);
        }

//            if ($prevOrder->date) {
//                orders::where('id', $newOrder->id)->update([
////                    'time' => $prevOrder->time
//                'date' => \request('date')
//                ]);
//
//            }

        if (\request('date')&&$prevOrder->date) {
            orders::where('id', $newOrder->id)->update([
//                    'time' => $prevOrder->time
                'date' => \request('date')
            ]);

        }

        elseif ($prevOrder->is_fast) {
                orders::where('id', $newOrder->id)->update([
                    'is_fast' => 1
                ]);
            }


            $order = orders::where('id', $newOrder->id)->get();

            //return back();

            return $this->sendResponse($order, 'Resend order successfully.');

    }











    /////////////////////// Driver Function ////////////////////



    //get all pending order
    public function allPendingOrder(){
        $pendingOrders = orders::where('status','is_not_confirm')->get();

        if(!$pendingOrders->toArray()){
           return $this->sendResponse($pendingOrders->toArray(), 'No Pending Orders Yet.');
        }else{
           return $this->sendResponse($pendingOrders->toArray(), 'Show Pending Orders successfully.');
        }

    }


    //Confirm Order
    public function confirmOrder($id){
         orders::where('id',$id)->update([
                'status' => 'is_confirm',
                'driver_id' => auth()->user()->id,
            ]);


        $order = orders::where('id',$id)->get();
        //return back();
        return $this->sendResponse($order, 'Confirm Order successfully.');
    }




    //Show Confirmed Orders For Driver
    public function showConfirmedOrderForDeriver(){
        $confirmedOrder =  orders::where('driver_id',auth()->user()->id)->get();
        if(!$confirmedOrder->toArray()){
          return $this->sendResponse($confirmedOrder->toArray(), 'No Confirmed Order Yet.');
        }else{
        return $this->sendResponse($confirmedOrder->toArray(), 'Show Confirmed Order successfully.');
        }

    }



    //Delivery Order To Client
    public function deliveryOrder($id){

        $order = orders::where('id',$id)->first();

        ///dd($order);

        $totalFines = \DB::table('orders')
            ->join('fines', 'orders.id', '=', 'fines.order_id')
            ->where(['orders.user_id'=>$order->user_id,'is_payment'=>0])
            ->sum('fine_value');


        $weight = \DB::table('orders')
            ->join('weights', 'weights.id', '=', 'orders.weight_id')
            ->where(['orders.id'=>$id])
            ->first();

        $weightPrice =$weight->price;

        $invoice = invoices::create([
          'order_id'=>$id,
          'total_cost'=>$weightPrice,
          'total_fines'=>$totalFines,
       ]);


        $detailsOfInvoice =  \DB::table('orders')
            ->join('invoices', 'invoices.order_id', '=', 'orders.id')
            ->where(['invoices.id'=>$invoice->id])
            ->get();


         return $this->sendResponse($detailsOfInvoice->toArray(), 'Show Details Of Invoice successfully.');


    }

    public function editLatLangDriver(Request $request)
    {
        $data = Validator::make($request->all(), [
            'current_lng'=>'required',
            'current_lat'=>'required'

            ]);
        if ($data->fails())
        {
            return $this->sendError('Validation Error.', $data->errors());
        }

          $edited =  DB::table('users')
                ->where('id', '=', Auth::user()->id)
                ->update([
                    'current_lng' => $request->current_lng,
                    'current_lat' => $request->current_lat,
                ]);

        return $this->sendResponse($edited, 'Edited latlng successfully.');
    }

    public function registerUser(Request $request){

        $data = Validator::make($request->all(), [
            'full_name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255|unique:users',
            'password'=>'required|string|min:8|confirmed',
            'phone'=>'required|numeric',
            'photo'=>'required',

        ]);

        if($data->fails())
        {
            return $this->sendError('Validation Error.', $data->errors());
        }

        $timeFolder = time();
        $user =  User::create([
            'full_name' => $request->full_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'is_accepted' => 1,
            'user_type' => 2,
        ]);



        if(request('photo')){
            User::where('id',$user->id)->update([
                'photo' => $request->file("photo")->store('photo/profile/'.$user->id),
            ]);
        }

        Auth::login($user);

        $access_token=$user->createToken('authToken')->accessToken; //to make user auth
        $this->token=$access_token;



        return $this->sendResponseWithToken($user->toArray(),$access_token,'user register successfully.');



    }
    public function editUserAccount($id)
    {
        $data = Validator::make(\request()->all(), [
            'full_name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255',
            'password'=>'required|string|min:8',
            'phone'=>'required|numeric',
            'photo'=>'required',

        ]);

        if($data->fails())
        {
            return $this->sendError('Validation Error.', $data->errors());
        }
        $user = DB::table('users')->where('id',$id)->update([
            'full_name' =>\request('full_name'),
            'email' => \request('email'),
            'password'=> Hash::make(\request('password')),
            'phone' => \request('phone'),
         ]);


        if(\request('photo'))
        {
            $user = DB::table('users')->where('id',$id)->update(
                [
                    'photo' => \request()->file("photo")->store('photo/profile/'.$id)
                ]
            );
        }

        return $this->sendResponse($user, 'edit account successfully.');
    }

    public function editDriverAccount($id){
        $data = Validator::make(\request()->all(), [
            'full_name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255',
            'password'=>'required|string|min:8',
            'phone'=>'required|numeric',
            //'photo'=>'image',
            'front_identification_photo'=>'required',
            'back_identification_photo'=>'required',
            'transportation_id'=>'required',
            //  'current_lng'=>'required',
            //  'current_lat'=>'required',

        ]);

        if($data->fails())
        {
            return $this->sendError('Validation Error.', $data->errors());
        }
        $timeFolder = time();
      $driver =  DB::table('users')->where('id',$id)->update([

            'full_name' => \request('full_name'),
            'email' =>\request('email'),
            'password' => Hash::make(\request('password')),
            'phone' => \request('phone'),
            'front_identification_photo' => \request()->file("front_identification_photo")->store('image_front_'.$timeFolder),
            'back_identification_photo' => \request()->file("back_identification_photo")->store('image_back_'.$timeFolder),
            'transportation_id' => \request('transportation_id'),
            'is_accepted' => 0,
            'user_type' => 1,
        ]);

        if(\request('photo'))
        {
          $driver =   DB::table('users')->where('id',$id)->update([
                'photo' => \request()->file("photo")->store('photo/profile/'.$id),
            ]);
        }
        return $this->sendResponse($driver, 'edit account successfully.');
    }

    public function registerDriver(Request $request){
        $data = Validator::make($request->all(), [
            'full_name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255|unique:users',
            'password'=>'required|string|min:8|confirmed',
            'phone'=>'required|numeric',
            //'photo'=>'image',
            'front_identification_photo'=>'required',
            'back_identification_photo'=>'required',
            'transportation_id'=>'required',
            //  'current_lng'=>'required',
            //  'current_lat'=>'required',

        ]);

        if($data->fails())
        {
            return $this->sendError('Validation Error.', $data->errors());
        }

        $timeFolder = time();
        $user =  User::create([
            'full_name' => $request->full_name,
            'email' =>$request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'front_identification_photo' => $request->file("front_identification_photo")->store('image_front_'.$timeFolder),
            'back_identification_photo' => $request->file("back_identification_photo")->store('image_back_'.$timeFolder),
            'transportation_id' => $request->transportation_id,
            'is_accepted' => 0,
            'user_type' => 1,
            // 'current_lng' => $request->current_lng,
            // 'current_lat' => $request->current_lat,
        ]);

        if(request('photo')){
            User::where('id',$user->id)->update([
                'photo' => $request->file("photo")->store('photo/profile/'.$user->id),
            ]);
        }



        // //To Rename Front Image
        // $newNameFrontImg = str_replace('image_front_'.$timeFolder , 'photo/frontIdent/'.$user->id, $user['front_identification_photo']);
        // \Storage::rename($user['front_identification_photo'] , $newNameFrontImg);
        // User::where('id' , $user->id )->update(['front_identification_photo'=>$newNameFrontImg]);
        // \Storage::deleteDirectory('image_front_'.$timeFolder);


        // //To Rename Back Image
        // $newNameBackImg = str_replace('image_back_'.$timeFolder , 'photo/backIdent/'.$user->id, $user['back_identification_photo']);
        // \Storage::rename($user['back_identification_photo'] , $newNameBackImg);
        // User::where('id' , $user->id )->update(['back_identification_photo'=>$newNameBackImg]);
        // \Storage::deleteDirectory('image_back_'.$timeFolder);

        Auth::login($user);

        $access_token=$user->createToken('authToken')->accessToken; //to make driver auth
        $this->token=$access_token;



        return $this->sendResponseWithToken($user->toArray(),$access_token,'driver register successfully.');

//
//        return redirect('/');
    }

}
