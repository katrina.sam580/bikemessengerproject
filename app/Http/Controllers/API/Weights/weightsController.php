<?php

namespace App\Http\Controllers\API\Weights;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\weights;


use App\Http\Controllers\API\BaseController as BaseController;

class weightsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $weights = weights::get();
        // dd($weights);

        if(!$weights->toArray()){
           return $this->sendResponse($weights->toArray(), 'No Weights Yet.');
        }else{
           return $this->sendResponse($weights->toArray(), 'Show Weights successfully.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $data = Validator::make(request()->all(), [
                  'name'=>'required|string|max:255',
              ]);

         if($data->fails()){
            return $this->sendError('Validation Error.', $data->errors());
        }



          $weight = weights::create([
           'name' => request()->name,

        ]);
       // return back();

        return $this->sendResponse($weight->toArray(), 'Weight Added successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $data = Validator::make(request()->all(), [
                  'name'=>'required|string|max:255',
          ]);

           if($data->fails()){
            return $this->sendError('Validation Error.', $data->errors());
        }

         weights::where('id',$id)->update([
           'name' => request()->name,

        ]);


         $weight =  weights::where('id',$id)->get();
       // return back();

        return $this->sendResponse($weight, 'Weight Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $weight = weights::where('id',$id)->first();
         weights::find($id)->delete();
        return $this->sendResponse($weight, 'Weight Deleted successfully.');
    }
}
