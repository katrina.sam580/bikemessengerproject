<?php

namespace App\Http\Controllers\API\Fines;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\fines;

class finesController extends BaseController
{
     
    
    
     /////////////////////// Client Function ////////////////////
    
    //Show All Fines For Client
    public function showFines(){
             
         $fines = \DB::table('orders')
            ->join('fines', 'orders.id', '=', 'fines.order_id')
            ->where(['user_id'=>auth()->user()->id,'is_payment'=>0])
            ->get(); 
        
         //dd($fines);
        
           
        if(!$fines->toArray()){
           return $this->sendResponse($fines->toArray(), 'No Fines Yet.');
        }else{
           return $this->sendResponse($fines->toArray(), 'Show Fines successfully.');
        }
        

    }
    
    
    
    
    
    
     /////////////////////// Admin Function ////////////////////
    
       
    //Show All Fines For Admin
    public function viewAllFines(){
        $fines = fines::get();
        
       // dd($fines);
        if(!$fines->toArray()){
           return $this->sendResponse($fines->toArray(), 'No Fines Yet.');
        }else{
           return $this->sendResponse($fines->toArray(), 'Show Fines successfully.');
        }
        

    }    
    
    
    
    
}
