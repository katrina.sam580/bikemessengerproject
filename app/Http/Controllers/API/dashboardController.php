<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


use App\invoices;
use App\orders;
use App\weights;
use App\User;
use App\fines;
use App\transportations;


use App\Http\Controllers\API\BaseController as BaseController;


class dashboardController extends BaseController
{
    
    //Show All Clients
    public function viewAllClients(){
       $clients =  User::where('user_type','is_user')->get();
        
        if(!$clients->toArray()){
           return $this->sendResponse($clients->toArray(), 'No Clients Yet.');
        }else{
           return $this->sendResponse($clients->toArray(), 'Show Clients successfully.');
        }
        
        
        
    }
    
    
    
    //Show All Drivers
    public function viewAllDrivers(){
        $drivers = User::where('user_type','is_driver')->get();
        
        if(!$drivers->toArray()){
           return $this->sendResponse($drivers->toArray(), 'No Drivers Yet.');
        }else{
           return $this->sendResponse($drivers->toArray(), 'Show Drivers successfully.');
        }
        

        
    }
    
    
    
    //Delete Client
    public function delClient($id){
        $client = User::where('id',$id)->first();
        User::find($id)->delete();
       return $this->sendResponse($client, 'Client Deleted successfully.');
    }
 
    
    //Accept Driver
    public function acceptDriver($id){
        User::where('id',$id)->update([
            'is_accepted' => 1
        ]);
        
        $driver =  User::where('id',$id)->first();
        
     return $this->sendResponse($driver, 'Driver Accepted successfully.');

    }
    
    
    //Show All Orders
    public function viewAllOrders(){
        $orders = orders::get();
       // dd($orders);
        
        
        if(!$orders->toArray()){
           return $this->sendResponse($orders->toArray(), 'No Orders Yet.');
        }else{
           return $this->sendResponse($orders->toArray(), 'Show Orders successfully.');
        }
        
    }
    
    
    
    
  
}
