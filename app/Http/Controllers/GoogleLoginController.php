<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class GoogleLoginController extends Controller
{
     public function redirectToGoogle(){
        return \Socialite::driver('google')->redirect();
 }
    

     
 
 public function handleGoogleCallback(){
        
        $userSocial = \Socialite::driver('google')->stateless()->user();
       
        $finduser = User::where('email',$userSocial->email)->first();
     
        // dd($finduser);
        if($finduser){
            
          \Auth::login($finduser);
          return \Redirect::to('/');
           // return "old google account";
            
        }else{
            $user = new User;
            
            $user->full_name = $userSocial->name;
            
            if($userSocial->email){
               $user->email = $userSocial->email; 
            }else{
                $timeFolder = time();
                $user->email = 'test_'.$timeFolder;
            }
      
            $user->photo = $userSocial->avatar_original;
            $user->password = bcrypt(123456);
            
            $user->save();
            
            //dd($userSocial->avatar_original);
            
            //To Store Image And Rename It
      /*      $img = file_get_contents($user->photo);
            $save = file_put_contents('sum',$img);
            
            User::where('id',$user->id)->update([
                'photo' => $save
            ]);*/ 
            
            
          /*  \Storage::download($user->id);
         User::where('id',$user->id)->update([
                'photo' => $user->photo->store($user->id)
            ]);*/
        
            
            

            
            
          \Auth::login($user);
          return \Redirect::to('/');
          //return "new google account";
            
            
            
        }

  
}
}
