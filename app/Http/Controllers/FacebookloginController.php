<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class FacebookloginController extends Controller
{
    
 public function redirectToFacebook(){
        return \Socialite::driver('facebook')->redirect();
 }
    

     
 
 public function handleFacebookCallback(){
        
        $userSocial = \Socialite::driver('facebook')->user();
       
        $finduser = User::where('email',$userSocial->email)->first();
     
        // dd($finduser);
        if($finduser){
            
          \Auth::login($finduser);
          return \Redirect::to('/');
          //  return "old account";
            
        }else{
            $user = new User;
            
            $user->full_name = $userSocial->name;
            
            if($userSocial->email){
               $user->email = $userSocial->email; 
            }else{
                $timeFolder = time();
                $user->email = 'test_'.$timeFolder;
            }
      
            $user->photo= $userSocial->avatar_original;
            $user->password = bcrypt(123456);
            
            $user->save();
            
            
          \Auth::login($user);
          return \Redirect::to('/');
         //  return "new account";

            
            
            
        }
     
  
}
}
