<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';
    
        
    // check if authenticated, then redirect to pages Debended On Role
    protected function authenticated() {
     if(\Auth::check()){
            if (auth()->user()->user_type == 'is_user') {
            return redirect('Home/User');
           } 
         
            if (auth()->user()->user_type == 'is_driver') {
            return redirect('Home/Driver');
           } 
         
           if (auth()->user()->user_type == 'is_admin') {
            return redirect('Dashboard');
           } 
        }   
}
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
