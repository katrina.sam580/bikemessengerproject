<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    
/*    
     'full_name', 'email', 'password','phone','photo','front_identification_photo','back_identification_photo',
        'is_admin','is_accepted','user_type','transportation_id'
    */
    
    public function registerUser(){
        
        $data = Validator::make(request()->all(), [
            'full_name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255|unique:users',
            'password'=>'required|string|min:8|confirmed',
            'phone'=>'required|numeric|max:255',
            'photo'=>'image',

        ]);
        
        $timeFolder = time();
       $user =  User::create([
            'full_name' => request('full_name'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
            'phone' => request('phone'),
            'is_accepted' => 1,
            'user_type' => 2,
        ]);
        
            if(request('photo')){
            User::where('id',$user->id)->update([
                'photo' => request()->file("photo")->store('photo/profile/'.$user->id),
            ]);
        }
        
        
         \Auth::login($user);
        
        return redirect('/');
    }  
    
    
    
    public function registerDriver(){
             $data = Validator::make(request()->all(), [
            'full_name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255|unique:users',
            'password'=>'required|string|min:8|confirmed',
            'phone'=>'required|numeric|max:255',
            'photo'=>'image',
            'front_identification_photo'=>'required|image',
            'back_identification_photo'=>'required|image',
            'transportation_id'=>'required',

        ]);
        
       $timeFolder = time();
       $user =  User::create([
            'full_name' => request('full_name'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
            'phone' => request('phone'),
            'front_identification_photo' => request()->file("front_identification_photo")->store('image_front_'.$timeFolder),
            'back_identification_photo' => request()->file("back_identification_photo")->store('image_back_'.$timeFolder),
            'transportation_id' => request('transportation_id'),
            'is_accepted' => 0,
            'user_type' => 1,
        ]);
        
        if(request('photo')){
            User::where('id',$user->id)->update([
                 'photo' => request()->file("photo")->store('photo/profile/'.$user->id),
            ]);
        }  
        
    
        
         //To Rename Front Image
         $newNameFrontImg = str_replace('image_front_'.$timeFolder , 'photo/frontIdent/'.$user->id, $user['front_identification_photo']);
         \Storage::rename($user['front_identification_photo'] , $newNameFrontImg);
         User::where('id' , $user->id )->update(['front_identification_photo'=>$newNameFrontImg]);
         \Storage::deleteDirectory('image_front_'.$timeFolder);
         
        
         //To Rename Back Image
         $newNameBackImg = str_replace('image_back_'.$timeFolder , 'photo/backIdent/'.$user->id, $user['back_identification_photo']);
         \Storage::rename($user['back_identification_photo'] , $newNameBackImg);
         User::where('id' , $user->id )->update(['back_identification_photo'=>$newNameBackImg]);
         \Storage::deleteDirectory('image_back_'.$timeFolder);
        
        
        
         \Auth::login($user);// to login after register
        
        return redirect('/'); 
    }
    
    
    
    
    
}
