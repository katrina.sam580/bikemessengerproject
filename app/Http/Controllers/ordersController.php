<?php

namespace App\Http\Controllers\API\Admin;


use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\orders;
use App\fines;
use App\invoices;

class ordersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('client.orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
      $data = Validator::make(request()->all(), [
            
            'title'=>'required|string|max:255',
            'order_details'=>'required|string',
            'phone'=>'required|numeric',
            'weight_id'=>'required',
            'time'=>'string',
            'status'=>'string',
            'is_fast'=>'boolean',
        ]);
        
     //   dd(request()->is_fast);
        
        
       $order = orders::create([
           'title' => request()->title,
           'order_details' => request()->order_details,
           'phone' => request()->phone,
           'weight_id' => request()->weight_id,
           'user_id' => auth()->user()->id,
           'status' => 2,
        ]);  
        
        if(request('time')){
            orders::where('id',$order->id)->update([
                 'time' => request('time')
            ]);
        }  
    
        
        if(request()->is_fast){
            orders::where('id',$order->id)->update([
                 'is_fast' => 1
            ]);
        }

                
       // return back();
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
    
    
    
    
    
    //////////////////////////// My Function Added ////////////////////////////
    
    //Show Pending Orders
    public function showPendingOrders(){
       $pendingOrders = orders::where([
            'user_id' => auth()->user()->id,
            'status' => 'is_not_confirm',
        ])->get();
        
        
        return view('client.orders.pending',['pendingOrders'=> $pendingOrders]);
    } 
    
    //Show Prev Orders
    public function showPreviousOrders(){
       $prevOrders = orders::where([
            'user_id' => auth()->user()->id,
            'status' => 'is_confirm',
        ])->get();
        
        
        return view('client.orders.previous',['prevOrders'=> $prevOrders]);
    }
    
    
    
    //Delete Pending Order
    public function delPendingOrder($id){
        orders::find($id)->delete();
        return back();
    }
    
    //Edit Pending Order
    public function editPendingOrder($id){
        
           $data = Validator::make(request()->all(), [
            
            'title'=>'required|string|max:255',
            'order_details'=>'required|string',
            'phone'=>'required|numeric',
            'weight_id'=>'required',
            'time'=>'string',
            'status'=>'string',
            'is_fast'=>'boolean',
        ]);
        
        
 
        
        $order = orders::where('id',$id)->update([
           'title' => request()->title,
           'order_details' => request()->order_details,
           'phone' => request()->phone,
           'weight_id' => request()->weight_id,
        ]);
        
        
        
            if(request('time')){
            orders::where('id',$id)->update([
                 'time' => request('time')
            ]);
        }  
    
       // dd(request()->is_fast);
        if(request()->is_fast){
            orders::where('id',$id)->update([
                 'is_fast' => 1
            ]);
        }
        
       if(request()->is_fast == null){
            orders::where('id',$id)->update([
                 'is_fast' => 0
            ]);
        }
 
        
        return back();
    }
 
    
    //ReSend Order
    public function reSendOrder($id){
        $prevOrder = orders::where('id',$id)->first();
        //dd($order->title);
        
         $order = orders::create([
           'title' => $prevOrder->title,
           'order_details' => $prevOrder->order_details,
           'phone' => $prevOrder->phone,
           'weight_id' => $prevOrder->weight_id,
           'user_id' => auth()->user()->id,
           'status' => 2,
        ]);
        
        if($prevOrder->time){
            orders::where('id',$order->id)->update([
                 'time' =>$prevOrder->time
            ]);
        }  
    
        
        if($prevOrder->is_fast){
            orders::where('id',$order->id)->update([
                 'is_fast' => 1
            ]);
        }
        
        return back();
        
    }
    
    
    
    
    //Show Fines
    public function showFines(){
             
         $orders = \DB::table('orders')
            ->join('fines', 'orders.id', '=', 'fines.order_id')
            ->where('user_id',auth()->user()->id)
            ->get(); 
        
         //dd($orders);
        
        return view('client.fines.fines',['orders'=>$orders]);
    }
    
       
    
    //Show Invoices
    public function showInvoices(){
             
         $invoices = \DB::table('orders')
            ->join('invoices', 'orders.id', '=', 'invoices.order_id')
            ->where('user_id',auth()->user()->id)
            ->get(); 
        
         //dd($orders);
        
        return view('client.invoices.invoices',['invoices'=>$invoices]);
    }
    
    
    
    /////////////////////// Driver Function ////////////////////
    
    
    
    //get all pending order
    public function allPendingOrder(){
        $pendingOrders = orders::where('status','is_not_confirm')->get();
        
        return view('driver.orders.pending',['pendingOrders'=>$pendingOrders]);
    }
    
    
    //Confirm Order
    public function confirmOrder($id){
         orders::where('id',$id)->update([
                'status' => 'is_confirm'
            ]);
        
        return back();
    }
    
    
    
        /////////////////////// Admin Function ////////////////////

    
    
}
