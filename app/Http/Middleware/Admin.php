<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::guest() || auth()->user()->user_type != "is_admin"){
//             return redirect('/');
            return response()->json(['message'=>'you can not access .'],200);
        }elseif(auth()->user()->user_type == "is_admin"){
            return $next($request); 
        }
       
                
       return $next($request);
    }
}
