<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoices extends Model
{
    
    protected $fillable = ['total_cost','total_fines','order_id','is_send'];
    /*'invoice_date',*/
    
    
    public function order_id(){
            return $this->hasOne('App\orders' , 'id' , 'order_id');
    } 
    
}
