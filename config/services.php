<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, SparkPost and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    
    
    //My Added
    'facebook' => [
        'client_id' => '191205675490704',
        'client_secret' => '8b8ee4efdfe4935ed177cf95ce8fde5b',
        'redirect' => '/login/facebook/callback',
    ], 
    
    
       'google' => [
        'client_id' => '622092819036-f03vh2nbq1m9g3jcbsdn8b6u2jcnbtmb.apps.googleusercontent.com',
        'client_secret' => 'NU512Tzy1_liTT_-E_SxyENd',
        'redirect' => 'http://localhost:8000/login/google/callback',
    ],  
    

];
